#pragma once
#include <assert.h>
#include "UnorderedArray.h"
#include <iostream>

using namespace std;

template<class T>
class Stack
{
public:
	Stack(int size)
	{
		if (size)
		{
			mContainer = new UnorderedArray<T>(size);
		}
	}

	virtual ~Stack()
	{
		if (checkContainer() != false)
			mContainer->~UnorderedArray();
	}

	void push(T val)
	{
		mContainer->pushFront(val);
	}

	const T& top()
	{
		mTopElement = mContainer->getTop();
		return mTopElement;
	}

	void pop()
	{
		mContainer->popFront();
	}

	bool checkContainer()
	{
		if (mContainer->getSize() != NULL)
			return true;

		else
			return false;
	}

	void print()
	{
		while (checkContainer() != false)
		{
			top();
			cout << mTopElement << endl;
			pop();
		}
	}

private:
	UnorderedArray<T>* mContainer;
	T mTopElement;
};

