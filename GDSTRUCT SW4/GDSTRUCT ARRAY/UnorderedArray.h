#pragma once

#include <assert.h>
#include <iostream>

using namespace std;
//T - generic
//template classes will accept any type of data as its elements
template<class T>

class UnorderedArray
{
private:
	T* mArray;
	int mMaxSize; //the size that we are going to declare in initialization
	int mGrowSize; // this is the number of elements to be added everytime we add elements
	int mNumElements; //current size of the array

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		//create a temporary bigger array
		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

public:
	UnorderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			// mMaxSize = 8, int = 8 bytes
			memset(mArray, 0, sizeof(T) * mMaxSize);
			//        ((if statement) ? true : false)
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	~UnorderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
		{
			expand();
		}

		mArray[mNumElements] = value;
		mNumElements++;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index < mNumElements);
		return mArray[index];
	}

	int getSize()
	{
		return mNumElements;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;

		//more efficient to not resize array; better to have more than than less
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL);

		if (index >= mMaxSize) // given index is out of range
		{
			return;
		}

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual int linearSearch()
	{
		T value;

		cout << "Enter number to find: ";
		cin >> value;
		assert(mArray != NULL);

		for (int i = 0; i < getSize(); i++)
		{
			if (mArray[i] == value)
				return i;
		}

		return -1;
	}
};