#pragma once
#include <assert.h>
#include <iostream>

using namespace std;

template<class T>
class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~OrderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		//your code goes after this line
		int index = getIndex(value);

		mNumElements++;

		for (int j = index; j < mNumElements; j++)
			swap(value, mArray[j]);

	}

	int getIndex(T value)
	{
		for (int i = 0; i < mNumElements; i++)
		{
			if (value < mArray[i])
			{
				return i;
			}
		}

		return mNumElements;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL && index <= mMaxSize);

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}


	virtual int binarySearch(T val)
	{
		//your code goes after this line
		int min = 0;
		int max = mNumElements - 1;
		int middle = (min + max) / 2;
		mCtr = 0;

		while (min <= max)
		{
			mCtr++;

			if (val == mArray[middle])
			{
				isFound = true;
				return middle;
			}

			else if (val > mArray[middle])
				min = middle + 1;


			else if (val < mArray[middle])
				max = middle - 1;

			middle = (min + max) / 2;

		}

		isFound = false;
		return -1;
	}

	int getCtr()
	{
		return mCtr;
	}

	bool getIsFound()
	{
		return isFound;
	}

private:
	T* mArray;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;
	int mCtr;
	bool isFound;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

};