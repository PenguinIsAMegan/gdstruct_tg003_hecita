#include <iostream>

using namespace std;

void addNum(int num, int& sum)
{
	int add = num % 10;

	sum = sum + add;

	if (num != 0)
	{	
		int newNum = num / 10;
		
		addNum(newNum, sum);
	}

	else
		cout << "Total: " << sum;
}

void printFibonacci(int nthTerm, int& term1, int&term2)
{	
	int nextTerm = term1 + term2;

	if (term1 == 0)
		cout << "0 ";

	else
		cout << term1 << " ";

	term1 = term2;
	term2 = nextTerm;

	if (nthTerm > 1)
	{
		nthTerm--;
		printFibonacci(nthTerm, term1, term2);
	}
}

void checkPrime(int num, int possibleNum, int& div)
{
	if (num % div == 0)
		cout << "Number is not a Prime Number";

	else
	{
		div++;

		if (div > possibleNum)
			cout << "Number is a Prime Number!";

		else
			checkPrime(num, possibleNum, div);
	}
}

int menu()
{
	int choice;

	cout << "What would you like to do?" << endl << endl;
	cout << "[1] - Find of each digit within a number" << endl;
	cout << "[2] - Fibonacci Sequence" << endl;
	cout << "[3] - Check if number if a Prime Number or Not" << endl;
	cin >> choice;

	return choice;
}

int main()
{
	int num, nthTerm;
	int sum = 0;
	int term1 = 0;
	int term2 = 1;
	int div = 2;
	
	while (true)
	{
		switch (menu())
		{
		case 1:
			cout << endl << endl << "Enter num: ";
			cin >> num;
			addNum(num, sum);
			break;
		case 2: cout << endl << endl << "Enter nth term for Fibonacci Sequence: ";
			cin >> nthTerm;
			cout << endl << "Sequence: ";
			printFibonacci(nthTerm, term1, term2);
			break;
		case 3: cout << endl << endl << "Enter number to check: ";
			cin >> num;
			int possibleNum = num / 2;
			checkPrime(num, possibleNum, div);
			break;
		}

		cout << endl << endl;
		system("pause");
		system("cls");
	}

	cout << endl << endl;
	system("pause");

	return 0;
}