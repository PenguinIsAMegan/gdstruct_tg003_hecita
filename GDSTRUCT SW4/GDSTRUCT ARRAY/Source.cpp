#include <string>
#include <iostream>
#include "UnorderedArray.h"

using namespace std;

int main()
{
	UnorderedArray<int> grades(5);
	UnorderedArray<char> letters(26);
	UnorderedArray<float> measurements(420);

	grades.push(69);
	grades.push(420);
	grades.push(69);
	grades.push(420);
	grades.push(69);
	grades.push(69);
	grades.push(420);

	for (int i = 0; i < grades.getSize(); i++)
		cout << grades[i] << endl;

	

	cout << "Index: " << grades.linearSearch() << endl;
	
	system("pause");

	return 0;
}