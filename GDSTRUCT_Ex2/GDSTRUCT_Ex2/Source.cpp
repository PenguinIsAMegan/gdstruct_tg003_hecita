#include <iostream>

using namespace std;

void enterNum(int* num, int size)
{
	cout << "Enter 10 numbers (positive or negative): " << endl;
	
	for (int i = 0; i < size; i++)
		cin >> num[i];
}

void display(int* num, int size)
{
 	for (int i = 0; i < size; i++)
		cout << num[i] << " ";
}

void sort(int* num, int size)
{
	int choice;
	cout << endl << endl << "Sort by:\n [1] - Ascending\n [2] - Descending" << endl;
	cin >> choice;

	if (choice == 1)
	{
		for (int i = 0; i < size - 1; i++)
		{
			for (int j = 0; j < size - i - 1; j++)
			{
				if (num[j] > num[j + 1])
					swap(num[j], num[j + 1]);
			}
		}
	}

	else if (choice == 2)
	{
		for (int i = 0; i < size - 1; i++)
		{
			for (int j = 0; j < size - i - 1; j++)
			{
				if (num[j] < num[j + 1])
					swap(num[j], num[j + 1]);
			}
		}
	}
}

void find(int* num, int size)
{
	int find;
	int steps = 0;
	cout << endl << endl << "Enter number to find: ";
	cin >> find;

	for (int i = 0; i < size; i++)
	{
		if (num[i] == find)
		{
			cout << "Number found! It took " << steps << " steps." << endl << endl;
			break;
		}

		else
			steps++;
		

		if (i == size - 1 && num[i] != find)
			cout << "Number not found!" << endl << endl;
		
	}
}

int main()
{
	const int size = 10;
	int num[size];

	enterNum(num, size);

	cout << endl << endl << "Unsorted Array: " << endl;
	display(num, size);

	sort(num, size);
	
	cout << endl << endl << "Sorted Array: " << endl;
	display(num, size);

	find(num, size);

	cout << endl << endl;
	system("pause");
	return 0;
}