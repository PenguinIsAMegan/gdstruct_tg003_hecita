#pragma once
#include <assert.h>
#include "UnorderedArray.h"

template<class T>
class Queue
{
public:
	Queue(int size)
	{
		if (size)
		{
			mContainer = new UnorderedArray<T>(size);
		}
	}

	virtual ~Queue()
	{
		if (checkContainer() != false)
			mContainer->~UnorderedArray();
	}

	void push(T val)
	{
		mContainer->push(val);
	}

	const T& top()
	{
		mTopElement = mContainer->getTop();
		return mTopElement;
	}

	void pop()
	{
		mContainer->popFront();
	}

	bool checkContainer()
	{
		if (mContainer->getSize() != NULL)
			return true;

		else
			return false;
	}

	void print()
	{
		while (checkContainer() != false)
		{
			top();
			cout << mTopElement << endl;
			pop();
		}
	}

private:
	UnorderedArray<T>* mContainer;
	T mTopElement;
};

