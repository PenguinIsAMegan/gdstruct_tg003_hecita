#include <iostream>
#include "Queue.h"
#include "Stack.h"

using namespace std;

void displayTop(Queue<int>& queued, Stack<int>& stacked)
{
	int top;

	cout << "Top elements of sets: " << endl;
	
	top = queued.top();
	cout << "Queue: " << top << endl;

	top = stacked.top();
	cout << "Stack: " << top << endl;
}

void printThenEmpty(Queue<int>& queued, Stack<int>& stacked)
{
	cout << "Queue elements: " << endl;
	queued.print();
	cout << "Stack elements: " << endl;
	stacked.print();
}

void popElements(Queue<int>& queued, Stack<int>& stacked)
{
	cout << "You have popped the front elements." << endl;

	queued.pop();
	stacked.pop();

	cout << endl << endl;

	if (queued.checkContainer() != NULL && stacked.checkContainer() != NULL)
	{
		displayTop(queued, stacked);
	}
}

void pushElements(Queue<int>& queued, Stack<int>& stacked)
{
	int value;

	cout << "Enter number: ";
	cin >> value;

	queued.push(value);
	stacked.push(value);

	cout << endl << endl;

	displayTop(queued, stacked);
}

int showMenu()
{
	int choice;

	cout << "What do you want to do?" << endl;
	cout << "1 - Push elements" << endl;
	cout << "2 - Pop elements" << endl;
	cout << "3 - Print everything then empty set" << endl;
	cin >> choice;

	return choice;
}

int main()
{
	int size;

	cout << "Enter size for element sets: ";
	cin >> size;

	Queue<int> queued(size);
	Stack<int> stacked(size);
	
	cout << endl << endl;

	while (true)
	{
		switch (showMenu())
		{
		case 1:
			cout << endl << endl;
			pushElements(queued, stacked);
			break;
		case 2:
			cout << endl << endl;
			popElements(queued, stacked);
			break;
		case 3: 
			cout << endl << endl;
			printThenEmpty(queued, stacked);
			break;
		}

		cout << endl;
		system("pause");
		system("cls");
	}

	cout << endl;
	system("pause");
	return 0;
}