#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void displayVectors(UnorderedArray<int>& unordered, OrderedArray<int>& ordered, int size)
{
	cout << "Unordered contents: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << "   ";

	cout << "\nOrdered contents: ";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << "   ";
}


void removeElement(UnorderedArray<int>& unordered, OrderedArray<int>& ordered, int size)
{
	int input;

	cout << "Enter index to remove: ";
	cin >> input;

	unordered.remove(input);
	ordered.remove(input);

	cout << endl;

	displayVectors(unordered, ordered, size);
}

void searchElement(UnorderedArray<int>& unordered, OrderedArray<int>& ordered, int size)
{
	cout << "\n\nInput element to search: ";
	int input;
	cin >> input;
	cout << endl;

	int result = unordered.linearSearch(input);

	if (result >= 0)
		cout << "Linear Search took " << unordered.getCtr() << " comparisons." << endl;

	result = ordered.binarySearch(input);

	if (result >= 0)
		cout << "Binary Search took " << ordered.getCtr() << " comparisons." << endl;

	if (unordered.getIsFound() == true && ordered.getIsFound() == true)
		cout << "Element " << input << " found. Index " << unordered.linearSearch(input) << " for Unordered Array, index " << ordered.binarySearch(input) << " for OrderedArray." << endl;

	else if (unordered.getIsFound() == false && ordered.getIsFound() == false)
		cout << "Element " << input << " not found." << endl;

	else if (unordered.getIsFound() == true)
		cout << "Element " << input << " only found in Unordered Array." << endl;

	else
		cout << "Element " << input << " only found in Ordered Array." << endl;
}

void generateArray(UnorderedArray<int>& unordered, OrderedArray<int>& ordered, int size)
{

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	cout << endl;

	displayVectors(unordered, ordered, size);
}

int showMenu()
{
	int choice;

	cout << "What do you want to do?" << endl;
	cout << "1 - Remove element at index" << endl;
	cout << "2 - Search for element" << endl;
	cout << "3 - Expand and generate random value" << endl;

	cin >> choice;

	return choice;
}

void main()
{
	cout << "Enter size for dynamic array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	generateArray(unordered, ordered, size);

	cout << endl;

	system("pause");
	system("cls");

	while (true)
	{
		displayVectors(unordered, ordered, size);

		cout << endl << endl;

		switch (showMenu())
		{
		case 1:
			removeElement(unordered, ordered, size);
			break;
		case 2:
			searchElement(unordered, ordered, size);
			break;
		case 3:
			cout << "Input size of expansion: ";
			cin >> size;
			generateArray(unordered, ordered, size);
			break;
		}

		cout << endl << endl;
		system("pause");
		system("cls");
	}

	cout << endl << endl;
	system("pause");
}