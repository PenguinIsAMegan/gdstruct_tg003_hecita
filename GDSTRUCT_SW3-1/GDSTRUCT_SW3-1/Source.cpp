#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;

void inputMembers(string*& guildMembers, int& size)
{
	for (int i = 0; i < size; i++)
	{
		cout << "Member #" << i + 1 << ": ";
		cin >> guildMembers[i];
	}
}

void displayGuild(string& guildName, int& size, string*& guildMembers)
{
	cout << guildName << " Members: " << endl;
	for (int i = 0; i < size; i++)
	{
		cout << guildMembers[i] << endl;
	}
}

void renameMember(string*& guildMembers, int& size, string& guildName)
{
	string choice;

	displayGuild(guildName, size, guildMembers);

	cout << "Choose member to rename: ";
	cin >> choice;

	for (int i = 0; i < size; i++)
	{
		if (choice == guildMembers[i])
		{
			cout << "Renaming " << guildMembers[i] << "..." << endl;
			cout << "Enter new name: " << endl;
			cin >> guildMembers[i];

			cout << "New list: " << endl;
			displayGuild(guildName, size, guildMembers);
			break;
		}

		else if (i == size - 1 && choice != guildMembers[i])
			cout << "Member not found!" << endl;
	}
}

void addMember(string*& guildMembers, int& size, string& guildName)
{
	cout << "Adding member..." << endl;
	int newSize = size + 1;
	string* newArray = new string[newSize];

	for (int i = 0; i < size; i++)
	{
		newArray[i] = guildMembers[i];
	}

	cout << "Enter new member name: ";
	cin >> newArray[newSize - 1];

	delete[] guildMembers;
	guildMembers = NULL;
	size = newSize;

	guildMembers = new string[size];
	
	for (int i = 0; i < size; i++)
	{
		guildMembers[i] = newArray[i];
	}
	
	delete[] newArray;

	cout << "New list: " << endl;
	displayGuild(guildName, size, guildMembers);
}

void deleteMember(string*& guildMembers, int& size, string& guildName)
{
	string choice;

	displayGuild(guildName, size, guildMembers);

	cout << "Select member to delete: ";
	cin >> choice;

	for (int i = 0; i < size; i++)
	{
		if (choice == guildMembers[i])
		{
			for (int s = i; s < size - 1; s++)
			{
				swap(guildMembers[s], guildMembers[s + 1]);
			}
		}

		else if (i == size - 1 && choice != guildMembers[i])
			cout << "Member not found!" << endl;
	}

	int newSize = size - 1;
	string* newArray = new string[newSize];

	for (int i = 0; i < newSize; i++)
	{
		newArray[i] = guildMembers[i];
	}

	delete[] guildMembers;
	guildMembers = NULL;
	size = newSize;
	
	guildMembers = new string[size];

	for (int i = 0; i < size; i++)
	{
		guildMembers[i] = newArray[i];
	}

	delete[] newArray;

	cout << "New list: " << endl;
	displayGuild(guildName, size, guildMembers);
}
int main()
{
	string guildName;
	int size;
	int choice;

	cout << "Enter guild name: ";
	cin >> guildName;
	cout << "Enter number of members: " << endl;
	cin >> size;
	string* guildMembers = new string[size];

	inputMembers(guildMembers, size);
	cout << endl;
	displayGuild(guildName, size, guildMembers);

	system("pause");

	while (true)
	{
		system("cls");
		cout << "MENU" << endl << endl;
		cout << "1 - Display all members" << endl;
		cout << "2 - Rename a member" << endl;
		cout << "3 - Add a member" << endl;
		cout << "4 - Delete a member" << endl;
		cin >> choice;

		switch (choice)
		{
		case 1: displayGuild(guildName, size, guildMembers); break;
		case 2: renameMember(guildMembers, size, guildName); break;
		case 3: addMember(guildMembers, size, guildName); break;
		case 4: deleteMember(guildMembers, size, guildName); break;
		}
		system("pause");
	}
	system("pause");
	return 0;
}